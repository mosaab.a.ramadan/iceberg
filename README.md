# iceberg

Iceberg system uses vueifty as a UI Framework, Axios for RESTful communication, Day.js for date formating, and google maps client to handle the maps
Use the instructions below to run on your own env

## Project setup
```
npm install
```

### Compiles and hot-reloads for development
```
npm run serve
```

### Compiles and minifies for production
```
npm run build
```

### Lints and fixes files
```
npm run lint
```

### Customize configuration
See [Configuration Reference](https://cli.vuejs.org/config/).
