import ApiService from "./api.service";
const AgentService = {
  init() {
    ApiService.init();
  },
  data: {
    agentlist: null,
  },
  async getAgents() {
    if (this.data.agentlist != null) return this.data.agentlist;
    else {
      let result = await ApiService.query("Agents");
      this.data.agentlist = result.data.records;
      return result.data.records;
    }
  },
};
export default AgentService;
