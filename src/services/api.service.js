import Vue from "vue";
import axios from "axios";
import VueAxios from "vue-axios";

const ApiService = {
  init() {
    Vue.use(VueAxios, axios);
    Vue.axios.defaults.baseURL =
      "https://api.airtable.com/v0/appgykZBGTF92MnHu/";
      Vue.axios.defaults.headers.common[
        "Authorization"
      ] = `Bearer keyKgK3babRzvU0YO`;
  },

  query(resource, params) {
    return Vue.axios.get(resource, params).catch((error) => {
      // ////console.log(error);
      throw new Error(`[ICE] ApiService ${error}`);
    });
  },

  /**
   * Send the GET HTTP request
   * @param resource
   * @param slug
   * @returns {*}
   */
  get(resource, slug) {
    return Vue.axios.get(`${resource}/${slug}`).catch((error) => {
      throw new Error(`[ICE] ApiService ${error}`);
    });
  },

  /**
   * Set the POST HTTP request
   * @param resource
   * @param params
   * @returns {*}
   */
  post(resource, params) {
    return Vue.axios.post(`${resource}`, params);
  },
  /**
   * Send the UPDATE HTTP request
   * @param resource
   * @param slug
   * @param params
   * @returns {IDBRequest<IDBValidKey> | Promise<void>}
   */
  update(resource, slug, params) {
    return Vue.axios.patch(`${resource}/${slug}`, params);
  },

  /**
   * Send the PUT HTTP request
   * @param resource
   * @param params
   * @returns {IDBRequest<IDBValidKey> | Promise<void>}
   */
  put(resource, params) {
    return Vue.axios.put(`${resource}`, params);
  },

  /**
   * Send the DELETE HTTP request
   * @param resource
   * @returns {*}
   */
  delete(resource) {
    return Vue.axios.delete(resource).catch((error) => {
      throw new Error(`[ICE] ApiService ${error}`);
    });
  },
  getPostCode(lng,lat)
  {
    try {
        var postcoderequest = require('axios');
        return postcoderequest.get(`postcodes?lon=${lng}&lat=${lat}` , {
          headers: {},
          baseURL: `https://api.postcodes.io/`
        });
      } catch (error) {
        return error;
      }
  }
};

export default ApiService;
