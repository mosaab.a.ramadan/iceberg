import ApiService from "./api.service";
const ClientService = {
  init() {
    ApiService.init();
  },
  data: {
    clientlist: null,
  },
  async getClients() {
    if (this.data.clientlist != null) return this.data.clientlist;
    else {
      let result = await ApiService.query("Contacts");
      this.data.clientlist = result.data.records;
      return result.data.records;
    }
  },
  async createClient(payload)
  {
    var data = {'fields': payload};
    let result = await ApiService.post("Contacts",data);
    return result.data.id;
  }
};
export default ClientService;
