import ApiService from "./api.service";
const AppointmentService = {
    init()
    {
        ApiService.init();
    },
    async getAppointmentsByAgent(agentid)
    {
        let result = await ApiService.get(
            "Appointments", `?filterByFormula=agent_id%3D${agentid}`
          );
          console.log(result)
          return result.data.records;
    },

    async getPostCodeByCoordinates(lng,lat)
    {
        let result = await ApiService.getPostCode(lng,lat);
          console.log("long and lat post code is",result)
          return result;
        
    },
    async createAppointment(payload)
    {
      var data = {'fields': payload};
      console.log("appointment payload",data)
      let result = await ApiService.post("Appointments",data);
      return result.status;
    }

};
export default AppointmentService;
