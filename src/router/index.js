import Vue from 'vue'
import VueRouter from 'vue-router'
import UserSelect from '../views/UserSelect.vue'

Vue.use(VueRouter)

const routes = [
  {
    path: '/',
    name: 'userselect',
    component: UserSelect
  },
  {
    path: '/dashboard',
    redirect: '/dashboard',
    component: () => import(/* webpackChunkName: "about" */ '../views/Dashboard.vue'),
    children: [
      {
        path: '/dashboard',
        name: 'dashboard',
        component: () => import('../views/dashboard/home.vue'),
      },
      {
        path: '/appointments',
        name: 'appointments',
        component: () => import('../views/dashboard/appointments.vue'),
      },
      {
        path: '/newappointment',
        name: 'addappointment',
        component: () => import('../views/dashboard/addappointment.vue'),
      }
    ]
  }
]

const router = new VueRouter({
  mode: 'history',
  base: process.env.BASE_URL,
  routes
})

export default router
