import Vue from 'vue'
import App from './App.vue'
import './registerServiceWorker'
import router from './router'
import store from './store'
import vuetify from './plugins/vuetify'

import AgentService from "./services/agent.service";
import AppointmentService from "./services/appointment.service";
import ClientService from './services/client.service';

import * as dayjs from 'dayjs'
var isSameOrBefore = require('dayjs/plugin/isSameOrBefore')
dayjs.extend(isSameOrBefore)

import { Loader } from "@googlemaps/js-api-loader"
const loader = new Loader({
  apiKey: "AIzaSyBxd787Vt2VhmTvH9-H23dsgJMyn6VjOl8",
  version: "weekly",
});

Vue.prototype.$map = loader;

AgentService.init();
AppointmentService.init();
ClientService.init();

Vue.prototype.$agent = AgentService;
Vue.prototype.$appointment = AppointmentService;
Vue.prototype.$client = ClientService;
Vue.prototype.$date = dayjs;

Vue.config.productionTip = false

new Vue({
  router,
  store,
  vuetify,
  render: h => h(App)
}).$mount('#app')
